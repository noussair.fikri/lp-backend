package com.lp.backend.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.lp.backend.model.Product;
import com.lp.backend.repository.ProductRepository;


@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api") // http://localhost:8080/api/**
public class ProductController {
	
	@Autowired
	ProductRepository productRepository;
	
	@GetMapping("/productsByProductName/{productName}")
	public ResponseEntity<List<Product>> getProductsByProductName(@PathVariable String productName) {
		
		try {
			List<Product> products = productRepository.findByProductName(productName);
			return new ResponseEntity<>(products, HttpStatus.OK);
		}
		catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
		}
		
	}
	
	@GetMapping("/productsById/{id}")
	public ResponseEntity<Product> getProductsById(@PathVariable String id) {
		
		try {
			Optional<Product> product = productRepository.findById(id);
			if(product.isPresent())
				return new ResponseEntity<>(product.get(), HttpStatus.OK);
			else 
				return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
		}
		catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
	}


	@GetMapping("/products")
	public ResponseEntity<List<Product>> getProducts() {
		
		try {
			List<Product> products = productRepository.findAll();
			return new ResponseEntity<>(products, HttpStatus.OK);
		}
		catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
		}
		
	}
	
	@PostMapping("/products")
	public ResponseEntity<Product> addProduct(@RequestBody Product product) { 
		
		try { 
			Product _product = productRepository.insert(product);
			return new ResponseEntity<>(_product, HttpStatus.CREATED);
		}
		 catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		 }
	}
	
	
	
}
