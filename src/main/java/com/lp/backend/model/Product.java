package com.lp.backend.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection="products")
public class Product {
	
	@Id
	private String id;
	
	private String image;
	private String productName;
	private double productPrice;
	private String description;
	private boolean available;
	
	public Product() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Product(String id, String image, String productName, double productPrice, String description,
			boolean available) {
		super();
		this.id = id;
		this.image = image;
		this.productName = productName;
		this.productPrice = productPrice;
		this.description = description;
		this.available = available;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public double getProductPrice() {
		return productPrice;
	}

	public void setProductPrice(double productPrice) {
		this.productPrice = productPrice;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public boolean isAvailable() {
		return available;
	}

	public void setAvailable(boolean available) {
		this.available = available;
	}
	
}
