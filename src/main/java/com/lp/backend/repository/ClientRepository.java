package com.lp.backend.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.lp.backend.model.Client;

public interface ClientRepository extends MongoRepository<Client, String> {

}
