package com.lp.backend.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.lp.backend.model.Product;
public interface ProductRepository extends MongoRepository<Product, String> {
	List<Product> findByProductName(String productName);
}
